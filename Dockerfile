FROM node:12.22.6-buster

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

EXPOSE 9000

CMD ["yarn", "start"]