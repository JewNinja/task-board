import React, { useState, ReactNode } from 'react';
import { THEMES_NAMES, themes } from './themes';
import './styles.scss';

type Props = {
  children: ReactNode
}

type ThemeStateType = keyof typeof themes;

const {LIGHT, DARK} = THEMES_NAMES;

export const ThemeContext = React.createContext(themes.dark);

const WithTheme = ({children}: Props) => {
  const [currentTheme, setTheme] = useState<ThemeStateType>(LIGHT);

  return (
    <div className="with-theme">
      <div className="">
        {Object.entries(themes)
          .map(theme => ({name: theme[0], data: theme[1]}))
          .filter(theme => theme.name !== currentTheme)
          .map(theme => (
            <div
              key={theme.name}
              className="with-theme__theme"
              onClick={() => setTheme(theme.name === LIGHT ? LIGHT : DARK)}
              style={{
                background: theme.data.column.background,
                borderColor: theme.data.column.backgroundOnDraggingOver,
                color: themes[currentTheme].column.background
              }}
            >
              {`to ${theme.name}`}
            </div>
          ))}
      </div>
      <ThemeContext.Provider value={themes[currentTheme]}>
        {children}
      </ThemeContext.Provider>
    </div>
  );
}

export default WithTheme;
