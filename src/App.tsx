import React from 'react';
import TaskBoard from './components/TaskBoard';
import WithTheme from './WithTheme';

const App = () => {
  return (
    <WithTheme>
      <TaskBoard />
    </WithTheme>
  );
}

export default App;
