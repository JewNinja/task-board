export const STATUSES = {
  PLAN: 'PLAN',
  IN_PROGRESS: 'IN PROGRESS',
  TESTING: 'TESTING',
  DONE: 'DONE'
};

export const IMPORTANCES = {
  MUST: 5,
  SHOULD: 4,
  COULD: 3
};
