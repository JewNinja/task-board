import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import cx from 'classnames';
import { TaskType } from '../types';
import './styles.scss';

type Props = {
  task: TaskType,
  index: number,
  selectedTaskId?: string,
  setSelectedTask: (task: TaskType) => void
}

const Task = ({task, index, selectedTaskId, setSelectedTask}: Props) => {
  return (
    <div className="task-board-task__container" onClick={() => setSelectedTask(task)}>
      <Draggable key={task.id} draggableId={task.id} index={index}>
        {(provided, snapshot) => {
          return (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              className={cx(
                'task-board-task',
                `task-board-task--importance-${task.importance.toLowerCase()}${snapshot.isDragging || task.id === selectedTaskId ? '-dragging' : ''}`
              )}
              style={{...provided.draggableProps.style}}
            >
              <p className="task-board-task__id">{task.id}</p>
              <span className="task-board-task__title">{task.title}</span>
            </div>
          );
        }}
      </Draggable> 
    </div>
  );
}

export default Task;