import { ValueOf } from "../../types";
import { IMPORTANCES, STATUSES } from "./constants";

export type StatusType = ValueOf<typeof STATUSES>

export type ImportanceType = keyof typeof IMPORTANCES

export type TaskType = {
  id: string,
  title: string,
  firstname: string,
  lastname: string,
  status: StatusType,
  importance: ImportanceType,
  date: string
}