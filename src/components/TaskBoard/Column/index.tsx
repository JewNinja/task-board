import React, { useContext } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import cx from 'classnames';
import Task from '../Task';
import { TaskType } from '../types';
import './styles.scss';
import { IMPORTANCES } from '../constants';
import { ThemeContext } from '../../../WithTheme';

type Props = {
  status: string,
  tasks: Array<TaskType>,
  selectedTaskId?: string,
  setSelectedTask: (task: TaskType) => void
}

const Column = ({status, tasks, selectedTaskId, setSelectedTask}: Props) => {
  const theme = useContext(ThemeContext);

  const sortTasks = (a: TaskType, b: TaskType) => {
    return IMPORTANCES[b.importance] - IMPORTANCES[a.importance]
  }

  return (
    <div className="task-board-column__container">
      <h2>{status}</h2>
      <Droppable droppableId={status}>
        {(provided, snapshot) => {
          return (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            className={cx('task-board-column')}
            style={{
              background: snapshot.isDraggingOver
                ? theme.column.backgroundOnDraggingOver
                : theme.column.background
            }}
            >
            {tasks.sort(sortTasks).map((task, index) => (
              <Task
                key={task.id}
                task={task}
                index={index}
                selectedTaskId={selectedTaskId}
                setSelectedTask={setSelectedTask}
              />
            ))}
          </div>
        )}}
      </Droppable>
    </div>
  );
}

export default Column;