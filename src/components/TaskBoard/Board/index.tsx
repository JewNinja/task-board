import React, { useState } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import Column from '../Column';
import { STATUSES } from '../constants';
import mockData from '../mockData';
import TaskInfo from '../TaskInfo';
import { StatusType, TaskType } from '../types';
import './styles.scss';

export type TaskWithStateType = TaskType & {selected: boolean}

const statusList = Object.values(STATUSES);

const Board = () => {
  const [tasks, setTasks] = useState<TaskType[]>(mockData);
  const [selectedTask, setSelectedTask] = useState<TaskType | null>(null);

  const onDragStart = (result: any) => {
    setSelectedTask(tasks.find(task => task.id === result.draggableId) || null)
  };
  
  const onDragEnd = (result: any, de: any) => {
    if (result.destination === null) return;

    setTasks(tasks.map(task =>
      task.id === result.draggableId
        ? {...task, status: result.destination.droppableId}
        : task
    ))
  };

  return (
    <div className="task-board-board">
      <div className="task-board-board__board-body">
        <DragDropContext onDragEnd={onDragEnd} onDragStart={onDragStart}>
          {statusList.map((status: StatusType, index) => (
            <Column
              key={index}
              status={status}
              tasks={tasks.filter(task => task.status === status)}
              selectedTaskId={selectedTask?.id}
              setSelectedTask={setSelectedTask}
            />
          ))}
        </DragDropContext>
      </div>
      <TaskInfo selectedTask={selectedTask} />
    </div>
  );
}

export default Board;