import React, { useContext } from 'react';
import cx from 'classnames';
import { ThemeContext } from '../../../WithTheme';
import { TaskType } from '../types';
import './styles.scss';

type Props = {
  selectedTask: TaskType | null
}

const TaskInfo = ({selectedTask}: Props) => {
  const theme = useContext(ThemeContext);

  return (
    <div className="task-board-info" style={{background: theme.column.backgroundOnDraggingOver}}>
      {selectedTask &&
        <p className={cx('task-board-info__name', `task-board-info__name--${theme.column.textColor}`)}>
          {`${selectedTask.lastname}, ${selectedTask.firstname}`}
        </p>
      }
    </div>
  );
}

export default TaskInfo;