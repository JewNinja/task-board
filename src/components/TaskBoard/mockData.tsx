import { TaskType } from "./types";

const mockData: Array<TaskType> = [
  {
    id: 'TSK-0001',
    title: 'Make the test task',
    firstname: 'Stepan',
    lastname: 'Vorobiov',
    status: 'DONE',
    importance: 'SHOULD',
    date: '2021-11-23T17:00:00'
  },
  {
    id: 'TSK-0002',
    title: 'Send test task for review',
    firstname: 'Stepan',
    lastname: 'Vorobiov',
    status: 'DONE',
    importance: 'SHOULD',
    date: '2021-11-24T10:00:00'
  },
  {
    id: 'TSK-0003',
    title: 'Сheck the test task',
    firstname: 'Reviewer',
    lastname: 'Revieweroff',
    status: 'IN PROGRESS',
    importance: 'COULD',
    date: '2021-11-24T13:00:00'
  },
  {
    id: 'TSK-0004',
    title: 'Send a job offer',
    firstname: 'Reviewer',
    lastname: 'Revieweroff',
    status: 'PLAN',
    importance: 'MUST',
    date: '2021-11-24T14:00:00'
  },
];

export default mockData;