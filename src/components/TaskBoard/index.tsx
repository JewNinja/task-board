import React from 'react';
import Board from './Board';

const TaskBoard = () => {
  return (
    <div className="App">
      <Board />
    </div>
  );
}

export default TaskBoard;