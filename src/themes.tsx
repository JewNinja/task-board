export const THEMES_NAMES = {
  LIGHT: 'light',
  DARK: 'dark'
};

export const themes = {
  [THEMES_NAMES.LIGHT]: {
    column: {
      background: '#FFF3CA',
      backgroundOnDraggingOver: '#FFE691',
      textColor: 'dark'
    }
  },
  [THEMES_NAMES.DARK]: {
    column: {
      background: '#1B1A31',
      backgroundOnDraggingOver: '#010015',
      textColor: 'light'
    }
  }
};